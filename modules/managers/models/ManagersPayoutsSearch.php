<?php

namespace app\modules\managers\models;

use app\modules\managers\models;
use yii\data\ActiveDataProvider;
use yii\base\Model;

use Yii;

/**
 * This is the model class for table "managers".
 *
 * @property int $id
 * @property string $name
 * @property string $s_name
 * @property string $l_name
 * @property string $salary
 *
 * @property ManagersHistory[] $managersHistories
 */
class ManagersPayoutsSearch extends models\base\ManagersPayouts
{
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [[
              'manager_id',
              'salary',  
              'calls',
              'bonus',  
              'extra',
              'total',
              'date'  ], 'string'],
            ];
    }
    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        
        $query = models\base\ManagersPayouts::find()
                ->where(['manager_id'=>$this->manager_id]);
                
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
            //'pagination' => [
            //    'pageSize' => 5,
            //],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere(['=', 'manager_id', $this->manager_id]);
        
        $query->andFilterCompare('salary', $this->manager_salary);
        $query->andFilterCompare('calls', $this->manager_calls);        
        $query->andFilterCompare('bonus', $this->manager_bonus);
        $query->andFilterCompare('extra', $this->manager_extra);
        $query->andFilterCompare('total', $this->manager_total);
        
        return $dataProvider;
    }
}
