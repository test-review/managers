<?php

namespace app\modules\managers\models;

use app\modules\managers\models;
use yii\data\ActiveDataProvider;
use yii\base\Model;

use Yii;

/**
 * This is the model class for table "managers".
 *
 * @property int $id
 * @property string $name
 * @property string $s_name
 * @property string $l_name
 * @property string $salary
 *
 * @property ManagersHistory[] $managersHistories
 */
class ManagersPayoutsSummarySearch extends models\base\ManagersPayoutsView
{
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [[
              'manager_id',
              'manager_name',
              'manager_status_id',
              'manager_salary',  
              'manager_calls',
              'manager_bonus',  
              'manager_extra',
              'manager_total'], 'string'],
            ];
    }
    
    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        
        $query = models\base\ManagersPayoutsView::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
                //'pagination' => [
                //    'pageSize' => 5,
                //],
        ]);
        
        
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }        
        
        // grid filtering conditions
        $query->andFilterWhere(['=', 'manager_id', $this->manager_id]);
        $query->andFilterWhere(['=', 'manager_status_id', $this->manager_status_id]);
        $query->andFilterWhere(['like', 'manager_name', $this->manager_name]);
        
        $query->andFilterCompare('manager_salary', $this->manager_salary);
        $query->andFilterCompare('manager_calls', $this->manager_calls);
        
        $query->andFilterCompare('manager_bonus', $this->manager_bonus);
        $query->andFilterCompare('manager_extra', $this->manager_extra);
        $query->andFilterCompare('manager_total', $this->manager_total);
        
        return $dataProvider;
    }
}
