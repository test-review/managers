<?php

namespace app\modules\managers\models\base;

use Yii;

/**
 * This is the model class for table "managers_history".
 *
 * @property int $id
 * @property int $manager_id
 * @property int $status_id
 * @property int $calls
 * @property int $date
 *
 * @property ManagersStatus $status
 * @property Managers $manager
 */
class ManagersHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'managers_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manager_id'], 'required'],
            [['manager_id', 'status_id', 'calls', 'date'], 'integer'],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ManagersStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => Managers::className(), 'targetAttribute' => ['manager_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manager_id' => 'Manager ID',
            'status_id' => 'Status ID',
            'calls' => 'Calls',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(ManagersStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Managers::className(), ['id' => 'manager_id']);
    }
}