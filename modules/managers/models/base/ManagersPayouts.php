<?php

namespace app\modules\managers\models\base;

use Yii;

/**
 * This is the model class for table "managers_payouts".
 *
 * @property int $id
 * @property int $manager_id
 * @property string $salary
 * @property int $calls
 * @property string $bonus
 * @property string $extra
 * @property string $total
 * @property int $date
 *
 * @property Managers $manager
 */
class ManagersPayouts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'managers_payouts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manager_id', 'salary', 'calls', 'total', 'date'], 'required'],
            [['manager_id', 'calls', 'date'], 'integer'],
            [['salary', 'bonus', 'extra', 'total'], 'number'],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => Managers::className(), 'targetAttribute' => ['manager_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manager_id' => 'Manager ID',
            'salary' => 'Salary',
            'calls' => 'Calls',
            'bonus' => 'Bonus',
            'extra' => 'Extra',
            'total' => 'Total',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Managers::className(), ['id' => 'manager_id']);
    }
}