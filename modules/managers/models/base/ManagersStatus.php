<?php

namespace app\modules\managers\models\base;

use Yii;

/**
 * This is the model class for table "managers_status".
 *
 * @property int $id
 * @property string $name
 *
 * @property Managers[] $managers
 * @property Managers[] $managers0
 */
class ManagersStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'managers_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManagersStatusGeneral()
    {
        return $this->hasMany(Managers::className(), ['status_general' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManagersStatusWork()
    {
        return $this->hasMany(Managers::className(), ['status_general' => 'id']);
    }
}
