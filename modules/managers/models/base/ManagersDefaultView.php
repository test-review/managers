<?php

namespace app\modules\managers\models\base;

use Yii;

/**
 * This is the model class for table "managers_default_view".
 *
 * @property int $manager_id
 * @property string $manager_name
 * @property int $manager_status_id
 * @property string $manager_salary
 * @property int $manager_salary_date
 * @property string $manager_calls
 * @property int $manager_bonus_id
 * @property string $manager_bonus_value
 * @property string $manager_bonus
 * @property int $manager_extra_id
 * @property string $manager_extra
 * @property string $manager_total
 * @property int $history_status_id
 * @property int $history_date
 */
class ManagersDefaultView extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'managers_default_view';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manager_id', 'manager_status_id', 'manager_salary_date', 'manager_bonus_id', 'manager_extra_id', 'history_status_id', 'history_date'], 'integer'],
            [['manager_name'], 'string'],
            [['manager_salary', 'manager_salary_date', 'history_status_id'], 'required'],
            [['manager_salary', 'manager_calls', 'manager_bonus_value', 'manager_bonus', 'manager_extra', 'manager_total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'manager_id' => 'Manager ID',
            'manager_name' => 'Manager Name',
            'manager_status_id' => 'Manager Status ID',
            'manager_salary' => 'Manager Salary',
            'manager_salary_date' => 'Manager Salary Date',
            'manager_calls' => 'Manager Calls',
            'manager_bonus_id' => 'Manager Bonus ID',
            'manager_bonus_value' => 'Manager Bonus Value',
            'manager_bonus' => 'Manager Bonus',
            'manager_extra_id' => 'Manager Extra ID',
            'manager_extra' => 'Manager Extra',
            'manager_total' => 'Manager Total',
            'history_status_id' => 'History Status ID',
            'history_date' => 'History Date',
        ];
    }
}