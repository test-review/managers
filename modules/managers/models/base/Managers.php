<?php

namespace app\modules\managers\models\base;

use Yii;

/**
 * This is the model class for table "managers".
 *
 * @property int $id
 * @property string $name
 * @property string $s_name
 * @property string $l_name
 * @property int $status_id
 *
 * @property ManagersStatus $status
 * @property ManagersHistory[] $managersHistories
 * @property ManagersPayouts[] $managersPayouts
 * @property ManagersSalary[] $managersSalaries
 */
class Managers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'managers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status_id'], 'integer'],
            [['name', 's_name', 'l_name'], 'string', 'max' => 255],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ManagersStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            's_name' => 'S Name',
            'l_name' => 'L Name',
            'status_id' => 'Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(ManagersStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistory()
    {
        return $this->hasMany(ManagersHistory::className(), ['manager_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayouts()
    {
        return $this->hasMany(ManagersPayouts::className(), ['manager_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalary()
    {
        return $this->hasMany(ManagersSalary::className(), ['manager_id' => 'id']);
    }
}