<?php

namespace app\modules\managers\models\base;

use Yii;

/**
 * This is the model class for table "{{%managers_bonus_extra}}".
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $value
 *
 * @property ManagersBonusCategory $category
 */
class ManagersBonusExtra extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'managers_bonus_extra';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'name', 'value'], 'required'],
            [['category_id'], 'integer'],
            [['value'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ManagersBonusCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'name' => 'Name',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ManagersBonusExtraCategory::className(), ['id' => 'category_id']);
    }
}
