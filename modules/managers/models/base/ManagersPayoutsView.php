<?php

namespace app\modules\managers\models\base;

use Yii;

/**
 * This is the model class for table "managers_payouts_view".
 *
 * @property int $manager_id
 * @property string $manager_name
 * @property int $manager_status_id
 * @property string $manager_salary
 * @property string $manager_calls
 * @property string $manager_bonus
 * @property string $manager_extra
 * @property string $manager_total
 */
class ManagersPayoutsView extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'managers_payouts_view';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manager_id', 'manager_status_id'], 'integer'],
            [['manager_name'], 'string'],
            [['manager_salary'], 'required'],
            [['manager_salary', 'manager_calls', 'manager_bonus', 'manager_extra', 'manager_total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'manager_id' => 'Manager ID',
            'manager_name' => 'Manager Name',
            'manager_status_id' => 'Manager Status ID',
            'manager_salary' => 'Manager Salary',
            'manager_calls' => 'Manager Calls',
            'manager_bonus' => 'Manager Bonus',
            'manager_extra' => 'Manager Extra',
            'manager_total' => 'Manager Total',
        ];
    }
}
