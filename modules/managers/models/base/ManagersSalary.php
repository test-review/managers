<?php

namespace app\modules\managers\models\base;

use Yii;

/**
 * This is the model class for table "{{%managers_salary}}".
 *
 * @property int $id
 * @property int $manager_id
 * @property string $value
 * @property int $date
 *
 * @property Managers $manager
 */
class ManagersSalary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'managers_salary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manager_id', 'value', 'date'], 'required'],
            [['manager_id', 'date'], 'integer'],
            [['value'], 'number'],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => Managers::className(), 'targetAttribute' => ['manager_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manager_id' => 'Manager ID',
            'value' => 'Value',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Managers::className(), ['id' => 'manager_id']);
    }
}
