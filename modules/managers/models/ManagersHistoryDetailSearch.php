<?php

namespace app\modules\managers\models;

use app\modules\managers\models;
use yii\data\ActiveDataProvider;
use yii\base\Model;

use Yii;

/**
 * This is the model class for table "managers".
 *
 * @property int $id
 * @property string $name
 * @property string $s_name
 * @property string $l_name
 * @property string $salary
 *
 * @property ManagersHistory[] $managersHistories
 */
class ManagersHistoryDetailSearch extends models\base\ManagersHistory
{
    public $date_str;
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            
        ];
    }
    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        
        /* //Optimisation      
         * SELECT `managers_history`.* 
         * FROM `managers_history`, (SELECT `managers_history`.`id` FROM `managers_history` WHERE (`date` BETWEEN 1527811200 AND 1530316800)) `h` 
         * WHERE `managers_history`.`manager_id` = 1 AND `managers_history`.`id`=`h`.`id` 
         * ORDER BY `status_id` DESC
         */
        
        $query = models\base\ManagersHistory::find()
                ->where(['manager_id'=>$this->manager_id])
                ->andWhere([
                    'between', 
                    'date', 
                    strtotime(date(('Y-m'), strtotime($this->date_str))), 
                    strtotime(date(('Y-m-t'), strtotime($this->date_str)))
                ]);
                
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
            //'pagination' => [
            //    'pageSize' => 5,
            //],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        return $dataProvider;
    }
}
