<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Managers */

$this->title = trim($model->name.($model->s_name?' '.$model->s_name:'').($model->l_name?' '.$model->l_name:''));

$this->params['breadcrumbs'][] = ['label' => 'Managers',    'url' =>['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title,  'url' =>['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Payouts';
?>
<div class="managers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p class="text-right">
        <span class="pull-left">Payouts history by <b>month</b></span>
        <?= Html::a('Payouts summary', ['payouts-summary'],   ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Clear filters',   ['payouts', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => "<div style='margin:10px 0;'><span>Показаны результаты {begin}-{end} из {totalCount}</span></div>",    
        'pager' => [
            'class' => '\yii\widgets\LinkPager',
            'maxButtonCount' => 12,
            'firstPageLabel' => '&laquo;',
            'prevPageLabel' => '&lsaquo;',
            'nextPageLabel' => '&rsaquo;',
            'lastPageLabel' => '&raquo;',
        ],
        'columns' => [
            [
                'attribute' => 'salary',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-right'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) {                
                    return number_format($searchModel->salary, 2, ',', ' ');        
                },
            ],
            [
                'attribute' => 'calls',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-right'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) {                
                    return number_format($searchModel->calls, 0, '', ' ');        
                },
            ],
            [
                'attribute' => 'bonus',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-right'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) {                
                    return number_format($searchModel->bonus, 2, ',', ' ');        
                },
            ],
            [
                'attribute' => 'extra',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-right'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) {                
                    return number_format($searchModel->extra, 2, ',', ' ');        
                },
            ],
            [
                'attribute' => 'total',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-right'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) {                
                    return number_format($searchModel->total, 2, ',', ' ');        
                },
            ],            
            [
                'attribute' => 'date',
                'label' => 'Month',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-right'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'filter' => '',
                'value' => function ($searchModel) {
                    return Html::a(date('M Y', $searchModel->date),
                            [
                                'history-detail', 
                                'id' => $searchModel->manager_id, 
                                'date' => date('Y-m',$searchModel->date)
                            ]);
                },
            ],
        ],
    ]); ?>

</div>