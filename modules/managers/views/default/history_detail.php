<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Managers */

$this->title = trim($model->name.($model->s_name?' '.$model->s_name:'').($model->l_name?' '.$model->l_name:''));

$this->params['breadcrumbs'][] = ['label' => 'Managers',    'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title,  'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = ['label' => 'Payouts',     'url' => ['payouts', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $date;

?>
<div class="managers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p class="text-right">
        <span class="pull-left">History detail on <b><?=strtolower(date('F Y', strtotime($date)))?></b></span>
        <?= Html::a('Clear filters', ['history-detail', 'id' => $model->id, 'date' => $date], ['class' => 'btn btn-primary']) ?>
    </p>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'summary' => "<div style='margin:10px 0;'><span>Показаны результаты {begin}-{end} из {totalCount}</span></div>",    
        'pager' => [
            'class' => '\yii\widgets\LinkPager',
            'maxButtonCount' => 12,
            'firstPageLabel' => '&laquo;',
            'prevPageLabel' => '&lsaquo;',
            'nextPageLabel' => '&rsaquo;',
            'lastPageLabel' => '&raquo;',
        ],
        'columns' => [
            
            [
                'attribute' => 'date',
                'headerOptions' => ['class' => 'col-sm-9'],
                'contentOptions' => ['class' => 'col-sm-9'],
                'filterOptions' => ['class' => 'col-sm-9'],
                'format' => 'raw',
                'value' => function ($searchModel) {
                    return date('Y M d - H:i', $searchModel->date);
                },
            ],
            [
                'attribute' => 'calls',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-center'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) {                
                    return $searchModel->calls!=0?number_format($searchModel->calls, 0, '', ' '):'';        
                },
            ],
            [
                'attribute' => 'status_id',
                'label' => 'Status',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-center'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) use ($status) {
                    
                    $class='';
                    
                    switch($searchModel->status_id)
                    {
                        case 5: $class=" label-success"; break;
                        case 6: $class=" label-default"; break;    
                        case 7: $class=" label-primary"; break;
                        case 8: $class=" label-warning"; break;
                        case 9: $class=" label-danger"; break;
                    }
                    
                    return "<span style=\"min-width:100%; display:inline-block;\" class=\"label label-default label-as-badge{$class}\">{$status[$searchModel->status_id]}</span>";
                },
            ],
        ],
    ]); ?>

</div>