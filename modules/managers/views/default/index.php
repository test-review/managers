<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Managers';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="managers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p class="text-right">
        <span class="pull-left">Current month activity</span>
        <?= Html::a('Payouts summary', ['payouts-summary'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Clear filters',   ['index'],           ['class' => 'btn btn-primary']) ?>
    </p>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => "<div style='margin:10px 0;'><span>Показаны результаты {begin}-{end} из {totalCount}</span></div>",    
        'pager' => [
            'class' => '\yii\widgets\LinkPager',
            'maxButtonCount' => 12,
            'firstPageLabel' => '&laquo;',
            'prevPageLabel' => '&lsaquo;',
            'nextPageLabel' => '&rsaquo;',
            'lastPageLabel' => '&raquo;',
        ],
        'columns' => [
            [
                'attribute' => 'manager_id',
                'label' => 'ID',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
            ],
            [
                'attribute' => 'manager_name',
                'label' => 'Name',
                'headerOptions' => ['class' => 'col-sm-3'],
                'contentOptions' => ['class' => 'col-sm-3'],
                'filterOptions' => ['class' => 'col-sm-3'],
                'format' => 'raw',
                'value' => function ($searchModel) {
                    return Html::a($searchModel->manager_name, ['view', 'id'=>$searchModel->manager_id]);        
                },
            ],
            [
                'attribute' => 'manager_status_id',
                'label' => 'Level',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-center'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) use ($status) {
                    
                    $class='';
                    
                    switch($searchModel->manager_status_id)
                    {
                        case 1: $class=" label-default"; break;    
                        case 2: $class=" label-success"; break;
                        case 3: $class=" label-primary"; break;
                        case 4: $class=" label-warning"; break;
                    }
                    
                    return "<span style=\"min-width:100%; display:inline-block;\" class=\"label label-default label-as-badge{$class}\">{$status[$searchModel->manager_status_id]}</span>";
                },
            ],
            [
                'attribute' => 'manager_salary',
                'label' => 'Salary',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-right'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) {
                    return number_format($searchModel->manager_salary, 2, ',', ' ');
                },
            ],
            [
                'attribute' => 'manager_calls',
                'label' => 'Calls',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-right'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) {
                    return number_format($searchModel->manager_calls, 0, '', ' ');
                },
            ],
            [
                'attribute' => 'manager_bonus',
                'label' => 'Bonus',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-right'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) {
                    return number_format($searchModel->manager_bonus, 2, ',', ' ');
                },
            ],
            [
                'attribute' => 'manager_extra',
                'label' => 'Extra',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-right'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) {
                    return number_format($searchModel->manager_extra, 2, ',', ' ');
                },
            ],
            [
                'attribute' => 'manager_total',
                'label' => 'Total',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-right'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) {
                    return number_format($searchModel->manager_total, 2, ',', ' ');
                },
            ],
            [
                'attribute' => 'history_status_id',
                'label' => 'Status',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-center'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) use ($status) {
                    
                    $class='';
                    
                    switch($searchModel->history_status_id)
                    {
                        case 5: $class=" label-success"; break;
                        case 6: $class=" label-default"; break;    
                        case 7: $class=" label-primary"; break;
                        case 8: $class=" label-warning"; break;
                        case 9: $class=" label-danger"; break;
                    }
                    
                    return "<span style=\"min-width:100%; display:inline-block;\" class=\"label label-default label-as-badge{$class}\">{$status[$searchModel->history_status_id]}</span>";
                },
            ],
            [
                'attribute' => 'history_date',
                'label' => 'Last Update',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-right'],
                'filterOptions' => ['class' => 'col-sm-1 text-center'],
                'filter'=> '<span style="height:62px; padding: 6px 0px; line-height:32px;"><b>'.date('M Y').'</b></span>',
                'format' => 'raw',
                'value' => function ($searchModel) {
                    
                    $date=date('Y-m-d H:i', $searchModel->history_date);
                    return "<span style='white-space:nowrap;'>{$date}</span>";
                },
            ],
        ],
    ]); ?>
</div>