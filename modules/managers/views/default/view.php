<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Managers */

$this->title = trim($model->name.($model->s_name?' '.$model->s_name:'').($model->l_name?' '.$model->l_name:''));
$this->params['breadcrumbs'][] = ['label' => 'Managers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="managers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p class="text-right">
        <?= Html::a('Payouts',  ['payouts', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update',   ['update', 'id' => $model->id],  ['class' => 'btn btn-success']) ?>
        
        <?= Html::a('Delete',['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            's_name',
            'l_name',
        ],
    ]) ?>
    <br />
    <h2>Overall statistics, except for the current month</h2>
    <br />
    <p><b>Level:</b> <?=ucfirst($status[$sc->manager_status_id])?></p>
    <p><b>Salary:</b> <?=number_format($sc->manager_salary, 2, ',', ' ')?> <i>(since <?=date("d F Y", $sc->manager_salary_date)?>)</i></p>
    <p><b>Active bonus:</b> <?=ucfirst($bonus[$sc->manager_bonus_id]).' - '.$sc->manager_bonus_value?></p>
    <p><b>Calls:</b> <?=number_format($so->calls, 0, '', ' ')?></p>
    <p><b>Bonus:</b> <?=number_format($so->bonus, 2, ',', ' ')?></p>
    <p><b>Extra:</b> <?=number_format($so->extra, 2, ',', ' ')?></p>
    <p><b>Total:</b> <?=number_format($so->total, 2, ',', ' ')?></p>
    <br />
    <h2>Current month activity: <?=date('F Y')?></h2>
    
    <span>Calls total: <?=number_format($sc->manager_calls, 0, '', ' ')?>;</span>
    <span>Bonus: <?=number_format($sc->manager_bonus, 2, ',', ' ')?>;</span> 
    <span>Extra: <?=number_format($sc->manager_extra, 2, ',', ' ')?>;</span>
    <span>Possible payment: <?=number_format($sc->manager_total, 2, ',', ' ')?>;</span>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'summary' => "<div style='margin:10px 0;'><span>Показаны результаты {begin}-{end} из {totalCount}</span></div>",    
        'pager' => [
            'class' => '\yii\widgets\LinkPager',
            'maxButtonCount' => 12,
            'firstPageLabel' => '&laquo;',
            'prevPageLabel' => '&lsaquo;',
            'nextPageLabel' => '&rsaquo;',
            'lastPageLabel' => '&raquo;',
        ],
        'columns' => [
            
            [
                'attribute' => 'date',
                'headerOptions' => ['class' => 'col-sm-9'],
                'contentOptions' => ['class' => 'col-sm-9'],
                'filterOptions' => ['class' => 'col-sm-9'],
                'format' => 'raw',
                'value' => function ($searchModel) {
                    return date('Y M d - H:i', $searchModel->date);
                },
            ],
            [
                'attribute' => 'calls',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-center'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) {                
                    return $searchModel->calls!=0?number_format($searchModel->calls, 0, '', ' '):'';        
                },
            ],
            [
                'attribute' => 'status_id',
                'label' => 'Status',
                'headerOptions' => ['class' => 'col-sm-1'],
                'contentOptions' => ['class' => 'col-sm-1 text-center'],
                'filterOptions' => ['class' => 'col-sm-1'],
                'format' => 'raw',
                'value' => function ($searchModel) use ($status) {
                    
                    $class='';
                    
                    switch($searchModel->status_id)
                    {
                        case 5: $class=" label-success"; break;
                        case 6: $class=" label-default"; break;    
                        case 7: $class=" label-primary"; break;
                        case 8: $class=" label-warning"; break;
                        case 9: $class=" label-danger"; break;
                    }
                    
                    return "<span style=\"min-width:100%; display:inline-block;\" class=\"label label-default label-as-badge{$class}\">{$status[$searchModel->status_id]}</span>";
                },
            ],
        ],
    ]); ?>

</div>