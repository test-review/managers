<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Managers */

$manager_name=trim($model->name.($model->s_name?' '.$model->s_name:'').($model->l_name?' '.$model->l_name:''));

$this->title = 'Update Managers: ' . $manager_name;
$this->params['breadcrumbs'][] = ['label' => 'Managers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $manager_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="managers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
