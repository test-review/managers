
/**
 * Managers Payouts View
 */
CREATE OR REPLACE VIEW managers_payouts_view AS SELECT
`manager`.`id` `manager_id`,
CONCAT(`manager`.`name`, 
       IF(ISNULL(`manager`.`s_name`), '', CONCAT(' ',`manager`.`s_name`)),
       IF(ISNULL(`manager`.`l_name`), '', CONCAT(' ',`manager`.`l_name`))
      ) `manager_name`,
`manager`.`status_id` `manager_status_id`,
`salary`.`value` `manager_salary`,
`payouts`.`calls` `manager_calls`,
`payouts`.`bonus` `manager_bonus`,
`payouts`.`extra` `manager_extra`,
`payouts`.`total` `manager_total`

FROM `managers` `manager`

JOIN `managers_salary` `salary` ON `salary`.`id` = (SELECT MAX(`id`) FROM `managers_salary` WHERE `manager_id`=`manager`.`id`)
JOIN (SELECT `manager_id`, SUM(`calls`) `calls`, SUM(`bonus`) `bonus`, SUM(`extra`) `extra`, SUM(`total`) `total` FROM `managers_payouts` GROUP BY `manager_id`) `payouts` ON `payouts`.`manager_id` = `manager`.`id`