
/**
 * Managers Default View
 */
CREATE OR REPLACE VIEW managers_default_view AS SELECT
`manager`.`id` `manager_id`,
CONCAT(`manager`.`name`, 
       IF(ISNULL(`manager`.`s_name`), '', CONCAT(' ',`manager`.`s_name`)),
       IF(ISNULL(`manager`.`l_name`), '', CONCAT(' ',`manager`.`l_name`))
      ) `manager_name`,
`manager`.`status_id` `manager_status_id`,
`salary`.`value` `manager_salary`,
`salary`.`date` `manager_salary_date`,
`history`.`manager_calls`,
`bonus`.`id` `manager_bonus_id`,
IFNULL(`bonus`.`value`,0) `manager_bonus_value`,
(IFNULL(`bonus`.`value`,0)*`history`.`manager_calls`) `manager_bonus`,
`extra`.`id` `manager_extra_id`,
IFNULL(`extra`.`extra`, 0) `manager_extra`,
(IFNULL(`bonus`.`value`,0)*`history`.`manager_calls`+ IFNULL(`extra`.`extra`, 0) + `salary`.`value`) `manager_total`,
`history_last`.`status_id` `history_status_id`,
`history_last`.`date` `history_date`

FROM `managers` `manager`

JOIN `managers_salary` `salary` ON `salary`.`id` = (SELECT MAX(`id`) FROM `managers_salary` WHERE `manager_id`=`manager`.`id`)
JOIN (SELECT `manager_id`, SUM(`calls`) AS `manager_calls` FROM `managers_history` WHERE (`date` BETWEEN unix_timestamp(DATE_FORMAT(NOW(), "%Y-%m-01")) AND unix_timestamp(DATE_FORMAT(NOW(), "%Y-%m-01")+INTERVAL 1 MONTH)) GROUP BY `manager_id`) `history` ON `history`.`manager_id` = `manager`.`id`
JOIN `managers_history` `history_last` ON `history_last`.`id`= (SELECT `id` FROM `managers_history` WHERE `manager_id`=`manager`.`id` ORDER BY `date` DESC LIMIT 1)
JOIN `managers_bonus` `bonus` ON `bonus`.`id` = 1
LEFT JOIN `managers_bonus_extra` `extra` ON `extra`.`id` = (SELECT `id` FROM `managers_bonus_extra` WHERE `history`.`manager_calls` >= `value` ORDER BY `value` DESC LIMIT 1)