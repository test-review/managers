<?php
namespace app\modules\managers\commands;

use yii\console\Controller;
use yii\helpers\Console;
use app\modules\managers\models;

class DbController extends Controller
{
    private $names=[
        'm'=>[
            'Jeffrey',
            'James',
            'Ed',
            'Anthony',
            'Luke',
            'Simon',
            'Rodrigo',
            'Jimmi',
            'Leonardo',
            'Clifton',
            'Louis',
            'Daniel',
            'Ben',
            'Steven',
            'Gustaf',
        ],
        
        'f'=>[
            'Izzabella',
            'Rachel',
            'Thandie',
            'Angela',
            'Tessa',
            'Shannon',
            'Ingrid',
            'Jasmyn',
            'Katja',
            'Betty',
            'Olga',
            'Jackie',
            'Rinko',
            'Kiki',
            'Jennifer'
        ]
    ];
    
    private $last_names=[
        
        'Abbett',
        'Bareford',
        'Price',
        'Stroke',
        'Williams',
        'Simpson',
        'Thompson',
        'Hemsworth',
        'Woodward',
        'Rae',
        'Newton',
        'Sarafyan',
        'Alvarez',
        'Fisher',
        'McClarnon',
        'Ogg',
        'Vela',
        'Fares',
        'Cage',
        'Roberts',
        'Sanders',
        'Hopkins',
        'O\'Daniel',
        'Page',
        'Mills',
        'Apel',
        'Lopez',
        'Platt',
        'Gray',
        'Mizuno',
        'Lee',
        'Space'        
    ];
    
    public function actionFillBase()
    {
        echo "Fill base\n";
    }
    
    public function actionFillExtended($managers_count=50, $start_date='2015-01-01')
    {
        $this->stdout("Fill extended managers database.\n\n", Console::FG_GREEN);
        
        $this->addManagersStatus();
        
        echo "\n";
        
        $this->addManagers($managers_count);
        
        echo "\n";
        
        $this->addManagersSalary($start_date);        
        
        echo "\n";
        
        $this->addManagersBonus();        
        
        echo "\n";
        
        $this->addManagersBonusExtraCategory();        
        
        echo "\n";
        
        $this->addManagersBonusExtra();        
        
        echo "\n";
        
        $this->addManagersHistory($start_date);        
        
        echo "\n";
        
        $this->addManagersPayouts($start_date);
        
        echo "\n";
        
        $this->stdout("\nFill extended managers database complete.\n", Console::FG_GREEN);
    }
    
     /**
     * 
     * @param type $managers_count
     * @throws Exception
     */
    protected function addManagersStatus()
    {
        $model= new models\base\ManagersStatus();
        
        Console::hideCursor();
        
        $this->stdout("Generate managers status.\n", Console::FG_YELLOW);
                  
        $data=[
            //General
            ['newbie'],
            ['junior'],
            ['middle'],
            ['senior'],
            
            //Work
            ['online'],       
            ['offline'],
            ['lunch'],
            ['holiday'],
            ['skip'],
            ['dismissed'],
        ];
        
        Console::moveCursorUp();
        $this->stdout("Generate managers status.", Console::FG_YELLOW);
        $this->stdout(" [");$this->stdout("OK", Console::FG_GREEN);$this->stdout("]\n");
        
echo <<<OUT

    -General status-
        
        01 - newbie
        02 - junior
        03 - middle
        04 - senior
        
    -Work status-

        05 - online     - Онлайн (работает)
        06 - offline    - Оффлайн (рабочий день окончен)
        07 - lunch      - Обед
        08 - holiday    - Выходной
        09 - skip       - Пропуск работы (не работал)
        10 - dismissed  - Уволен
        
OUT;
        echo "\n";
        
        if (!empty($data) && is_array($data)) {

            try{
            
                $this->stdout("Apply managers status data.\n");
                
                $model::getDb()
                    ->createCommand()
                    ->batchInsert(
                            $model->tableName(),
                            [
                                'name'
                            ],
                            $data)
                    ->execute();
            }
            catch(yii\db\Exception $e)
            {
                throw new Exception($e->massege());
            }
            
            Console::moveCursorUp();
            $this->stdout("Apply managers status data.");
            $this->stdout(" [");$this->stdout("OK", Console::FG_GREEN);$this->stdout("]\n");
        }
        
        Console::moveCursorDown();
        Console::showCursor();
    }
    
    /**
     * 
     * @param type $managers_count
     * @throws Exception
     */
    protected function addManagers($managers_count)
    {       
        $model=new models\base\Managers();
        
        $data=[];
        
        Console::hideCursor();
        
        $status_id=[
            1,
            1,
            
            2,
            2,
            2,
            2,
            
            3,
            3,
            
            4
        ];        
        
        for($i=1; $i < $managers_count+1; $i++)
        {
            
            $gender=rand(0,1)==0?'m':'f';
        
            $data[]=[
                
                'status_id'=>$status_id[array_rand($status_id, 1)],
                'name'   => $this->names[$gender][array_rand($this->names[$gender], 1)],
                's_name' => rand(0,1)==1?$this->names[$gender][array_rand($this->names[$gender],1)]:null,
                'l_name' => $this->last_names[array_rand($this->last_names, 1)],
            ];
            
            $this->stdout("Generate names {$i} of {$managers_count} managers.\n", Console::FG_YELLOW);
            Console::moveCursorUp();
        }
        
        $this->stdout("Generate names {$managers_count} of {$managers_count} managers.", Console::FG_YELLOW);
        $this->stdout(" [");$this->stdout("OK", Console::FG_GREEN);$this->stdout("]\n");
        
        
        if (!empty($data) && is_array($data)) {
            
            $batch = [];

            foreach ($data as $manager) {
                $batch[] = [
                    $manager['status_id'],
                    $manager['name'], 
                    $manager['s_name'], 
                    $manager['l_name'], 
                ];
            }
            
            try{
            
                $this->stdout("Apply managers data.\n");
                
                $model::getDb()
                    ->createCommand()
                    ->batchInsert(
                            $model->tableName(),
                            [
                                'status_id',
                                'name',
                                's_name',
                                'l_name',
                            ],
                            $batch)
                    ->execute();
            }
            catch(yii\db\Exception $e)
            {
                throw new Exception($e->massege());
            }
            
            Console::moveCursorUp();
            $this->stdout("Apply managers data.");
            $this->stdout(" [");$this->stdout("OK", Console::FG_GREEN);$this->stdout("]\n");
        }
        
        Console::moveCursorDown();
        Console::showCursor();
    }
    
    /**
     * 
     */
    protected function addManagersSalary($start_date)
    {
        $model= new models\base\ManagersSalary();
        
        $data=[];
        
        Console::hideCursor();
        
        $this->stdout("Generate managers salary on {$start_date}.\n", Console::FG_YELLOW);
        
        $salaries=[
            1 => 10000, //newbie
            2 => 18000, //junior
            3 => 25000, //middle
            4 => 32000, //senior
        ];
        
        Console::moveCursorUp();
        $this->stdout("Generate managers salary on {$start_date}.", Console::FG_YELLOW);
        $this->stdout(" [");$this->stdout("OK", Console::FG_GREEN);$this->stdout("]\n");
        
echo <<<OUT

    -Salary-
        
        newbie  - 10 000
        junior  - 18 000
        middle  - 25 000
        senior  - 32 000
        
OUT;

        echo "\n";
        
        $managers = models\base\Managers::find()->select(['id', 'status_id'])->asArray()->all();
        
        $date = strtotime($start_date);
        
        foreach($managers as $manager)
        {            
            $data[]=[                
                $manager['id'],
                $salaries[$manager['status_id']],     
                $date
            ];            
        }
        
        if (!empty($data) && is_array($data)) {

            try{
            
                $this->stdout("Apply managers salary data.\n");
                
                $model::getDb()
                    ->createCommand()
                    ->batchInsert(
                            $model->tableName(),
                            [
                                'manager_id',
                                'value',
                                'date'
                            ],
                            $data)
                    ->execute();
            }
            catch(yii\db\Exception $e)
            {
                throw new Exception($e->massege());
            }
            
            Console::moveCursorUp();
            $this->stdout("Apply managers salary data.");
            $this->stdout(" [");$this->stdout("OK", Console::FG_GREEN);$this->stdout("]\n");
        }
        
        Console::moveCursorDown();
        Console::showCursor();
    }
        /**
     * 
     * @param type $managers_count
     * @throws Exception
     */
    protected function addManagersBonus()
    {
        $model= new models\base\ManagersBonus();
        
        Console::hideCursor();
        
        $this->stdout("Generate managers bonus.\n", Console::FG_YELLOW);
                  
        $data=[
            //General
            ['per one call', 10],
        ];
        
        Console::moveCursorUp();
        $this->stdout("Generate managers bonus.", Console::FG_YELLOW);
        $this->stdout(" [");$this->stdout("OK", Console::FG_GREEN);$this->stdout("]\n");
        
echo <<<OUT

    -Bonus-
        
        01 - per one call : 10.00
        
OUT;
        echo "\n";
        
        if (!empty($data) && is_array($data)) {

            try{
            
                $this->stdout("Apply managers bonus data.\n");
                
                $model::getDb()
                    ->createCommand()
                    ->batchInsert(
                            $model->tableName(),
                            [
                                'name',
                                'value'
                            ],
                            $data)
                    ->execute();
            }
            catch(yii\db\Exception $e)
            {
                throw new Exception($e->massege());
            }
            
            Console::moveCursorUp();
            $this->stdout("Apply managers bonus data.");
            $this->stdout(" [");$this->stdout("OK", Console::FG_GREEN);$this->stdout("]\n");
        }
        
        Console::moveCursorDown();
        Console::showCursor();
    }
    
    /**
     * 
     * @throws Exception
     */
    protected function addManagersBonusExtraCategory()
    {
        $model= new models\base\ManagersBonusExtraCategory();
        
        $data=[];
        
        Console::hideCursor();
        
        $this->stdout("Generate managers bonus extra category.\n", Console::FG_YELLOW);
        
        $data=[
            ['Начальная'],
            ['Средняя'],
            ['Высшая']
        ];
        
        Console::moveCursorUp();
        $this->stdout("Generate managers bonus extra category.", Console::FG_YELLOW);
        $this->stdout(" [");$this->stdout("OK", Console::FG_GREEN);$this->stdout("]\n");
        
echo <<<OUT

    -Bonus Extra Category-
        
        01 - Начальная
        02 - Средняя
        03 - Высшая        
        
OUT;

        echo "\n";
        
        if (!empty($data) && is_array($data)) {

            try{
            
                $this->stdout("Apply managers bonus extra category data.\n");
                
                $model::getDb()
                    ->createCommand()
                    ->batchInsert(
                            $model->tableName(),
                            [
                                'name',
                            ],
                            $data)
                    ->execute();
            }
            catch(yii\db\Exception $e)
            {
                throw new Exception($e->massege());
            }
            
            Console::moveCursorUp();
            $this->stdout("Apply managers bonus extra category data.");
            $this->stdout(" [");$this->stdout("OK", Console::FG_GREEN);$this->stdout("]\n");
        }        
        
        Console::moveCursorDown();
        Console::showCursor();
    }
        
    /**
     * 
     * @throws Exception
     */
    protected function addManagersBonusExtra()
    {
        $model= new models\base\ManagersBonusExtra();
        
        $data=[];
        
        Console::hideCursor();
        
        $this->stdout("Generate managers bonus extra.\n", Console::FG_YELLOW);
        
        $data[]=[ 1, 'От 100(включительно)',  100,  1000 ];
        $data[]=[ 1, 'От 200(включительно)',  200,  2000 ];
        $data[]=[ 1, 'От 300(включительно)',  300,  3000 ];
        $data[]=[ 1, 'От 400(включительно)',  400,  5000 ];
        $data[]=[ 2, 'От 500(включительно)',  500,  7000 ];
        $data[]=[ 2, 'От 600(включительно)',  600,  9000 ];
        $data[]=[ 2, 'От 700(включительно)',  700,  11000 ];
        $data[]=[ 2, 'От 800(включительно)',  800,  15000 ];
        $data[]=[ 3, 'От 900(включительно)',  900,  20000 ];
        $data[]=[ 3, 'От 1000(включительно) и более', 1000, 30000 ];
        
        Console::moveCursorUp();
        $this->stdout("Generate managers bonus extra.", Console::FG_YELLOW);
        $this->stdout(" [");$this->stdout("OK", Console::FG_GREEN);$this->stdout("]\n");
        
echo <<<OUT

    -Bonus Extra-
        
        01 - Extra : 1 000  [Начальная] 100 - 199
        02 - Extra : 2 000  [Начальная] 200 - 299
        03 - Extra : 3 000  [Начальная] 300 - 399
        04 - Extra : 5 000  [Начальная] 400 - 499
        
        05 - Extra : 7 000  [Средняя]   500 - 599  
        06 - Extra : 9 000  [Средняя]   600 - 699
        07 - Extra : 11 000 [Средняя]   700 - 799
        08 - Extra : 15 000 [Средняя]   800 - 899
        
        09 - Extra : 20 000 [Высшая]    900 - 999  
        10 - Extra : 30 000 [Высшая]    1000 и более      
        
OUT;

        echo "\n";
        
        if (!empty($data) && is_array($data)) {

            try{
            
                $this->stdout("Apply managers bonus extra data.\n");
                
                $model::getDb()
                    ->createCommand()
                    ->batchInsert(
                            $model->tableName(),
                            [
                                'category_id',
                                'name',
                                'value',
                                'extra'
                            ],
                            $data)
                    ->execute();
            }
            catch(yii\db\Exception $e)
            {
                throw new Exception($e->massege());
            }
            
            Console::moveCursorUp();
            $this->stdout("Apply managers bonus extra data.");
            $this->stdout(" [");$this->stdout("OK", Console::FG_GREEN);$this->stdout("]\n");
        }

        Console::moveCursorDown();
        Console::showCursor();        
    }
    
    /**
     * 
     * @throws Exception
     */
    protected function addManagersHistory($start_date)
    {
        $model= new models\base\ManagersHistory();
        
        $data=[];
        
        Console::hideCursor();
        
        $this->stdout("Generate managers history from {$start_date} to ".date('Y-m-d').".\n", Console::FG_YELLOW);
        
        $managers = models\base\Managers::find()->select(['id', 'status_id'])->asArray()->all();

        $period = new \DatePeriod(new \DateTime($start_date), new \DateInterval('P1D'), new \DateTime());

        $period_count=iterator_count($period);
        
        Console::startProgress(0, $period_count);
        
        $progress=0;
        
        foreach ($period as $current_day)
        {
            $data=[];
                            
            foreach($managers as $manager)
            {
                //start new day
                //foreach four from 9:00 to 18:00 breaktime 13:00-14:00, 
                //also manager can work without breaktime
              
                $day_of_week=$current_day->format('w');
                
                if($day_of_week > 0 && $day_of_week < 6)
                {                
                    $has_break_out=rand(0, 5);
                    $no_work=rand(0,10);
                    
                    if($no_work!=0)
                    {        
                        $data[]=[                
                            $manager['id'], //manager_id
                            5,              //status_id (online)
                            0,              //calls
                            strtotime($current_day->format('Y-m-d').' 9:00'), //date
                        ];
                                                
                        for($hour=9; $hour < 18; $hour++)
                        {
                            //in one hour manager can takes max 7 or min 0 calls

                            if($hour!=13 || ($hour==13 && $has_break_out==0))
                            {
                                $calls=0;
                                
                                switch ($manager['status_id'])
                                {
                                    case 1:
                                        $calls=rand(0, 4); //newbie
                                        break;
                                    case 2:
                                        $calls=rand(0, 7); //junior
                                        break;
                                    case 3:
                                        $calls=rand(0, 5); //middle
                                        break;
                                    case 4:
                                        $calls=rand(0, 3); //senior
                                        break;
                                }
                                
                                $data[]=[                
                                    $manager['id'], //manager_id
                                    5,              //status_id
                                    $calls,         //calls
                                    strtotime($current_day->format('Y-m-d').' '.$hour.':00'), //date
                                ];
                            }
                            else if($hour==13 && $has_break_out>0)
                            {
                                $data[]=[                
                                    $manager['id'], //manager_id
                                    7,              //status_id (lunch)
                                    0,              //calls
                                    strtotime($current_day->format('Y-m-d').' '.$hour.':00'), //date
                                ];
                            }
                        }
                        
                        $data[]=[                
                            $manager['id'], //manager_id
                            6,              //status_id (offline)
                            0,              //calls
                            strtotime($current_day->format('Y-m-d').' 18:00'), //date
                        ];
                    }
                    else
                    {
                        $data[]=[                
                            $manager['id'], //manager_id
                            9,              //status_id (skip)
                            0,              //calls
                            strtotime($current_day->format('Y-m-d').' 9:00'), //date
                        ];
                    }
                }
                else 
                {
                    //holliday
                    $data[]=[                
                        $manager['id'], //manager_id
                        8,              //status_id (holliday)
                        0,              //calls
                        strtotime($current_day->format('Y-m-d')), //date
                    ];
                }
            }
            
            //save to db     
            
            if (!empty($data) && is_array($data)) {

                try{

                    $model::getDb()
                        ->createCommand()
                        ->batchInsert(
                                $model->tableName(),
                                [
                                    'manager_id',
                                    'status_id',
                                    'calls',
                                    'date'
                                ],
                                $data)
                        ->execute();
                }
                catch(yii\db\Exception $e)
                {
                    throw new Exception($e->massege());
                }
            }
            
            $progress++;
            Console::updateProgress($progress, $period_count);
        }    
        
        Console::endProgress();
        
        //Console::moveCursorUp();
        $this->stdout("Apply managers history data.");
        $this->stdout(" [");$this->stdout("OK", Console::FG_GREEN);$this->stdout("]\n");
        
        Console::moveCursorDown();
        Console::showCursor();
    }
    
    /**
     * 
     * @throws Exception
     */
    protected function addManagersPayouts($start_date)
    {
        $model= new models\base\ManagersPayouts();
        
        $data=[];
        
        Console::hideCursor();
        
        $this->stdout("Calculate managers payouts from ".date('Y-m', strtotime($start_date))." to ".date('Y-m', strtotime('-1 month')).".\n", Console::FG_YELLOW);
        
        $managers = models\base\Managers::find()->select(['id', 'status_id'])->asArray()->all();

        $period = new \DatePeriod(new \DateTime(date('Y-m', strtotime($start_date))), new \DateInterval('P1M'), new \DateTime(date('Y-m')));

        $period_count=iterator_count($period);
        
        Console::startProgress(0, $period_count);
        
        $progress=0;
        
        foreach ($period as $current_month)
        {
            $data=[];
             
            $salary_sql = models\base\ManagersSalary::find()
                    ->select('MAX(`id`) `id`')
                    ->where(['<=', 'date', strtotime($current_month->format('Y-m-t 23:59:59'))])
                    ->andWhere('`manager_id` = `manager`.`id`')
                    ->createCommand()->getRawSql();
            
            $history_sql = models\base\ManagersHistory::find()
                    ->select(['manager_id', 'SUM(`calls`) `manager_calls`'])
                    ->where([
                        'between', 
                        'date', 
                        strtotime($current_month->format('Y-m')), 
                        strtotime($current_month->format('Y-m-t 23:59:59'))])
                    ->groupBy('manager_id')
                    ->createCommand()->getRawSql();
            
            $extra_sql = models\base\ManagersBonusExtra::find()
                    ->select('id')
                    ->where('`history`.`manager_calls` >= `value`')
                    ->orderBy(['value'=>SORT_DESC])
                    ->limit(1)
                    ->createCommand()->getRawSql();            
            
            
            $managers = models\base\Managers::find()                    
                    ->select([
                        
                        '`manager`.`id` `manager_id`',
                        '`salary`.`value` `manager_salary`',
                        '`history`.`manager_calls`',
                        '(`bonus`.`value`*`history`.`manager_calls`) `manager_bonus`',
                        'IFNULL(`extra`.`extra`, 0) `manager_extra`',
                        '(`bonus`.`value`*`history`.`manager_calls`+ IFNULL(`extra`.`extra`, 0) + `salary`.`value`) `manager_total`'
                    ])
                    ->from('managers manager')
                    ->join('JOIN', 'managers_salary salary', '`salary`.`id` = ('.$salary_sql.')')
                    ->join('JOIN', '('.$history_sql.') `history`', '`history`.`manager_id` = `manager`.`id`')
                    ->join('JOIN', 'managers_bonus bonus', ['bonus.id'=>1])
                    ->join('LEFT JOIN', 'managers_bonus_extra extra', 'extra.id=('.$extra_sql.')')
                    ->asArray()
                    ->all();
            
            foreach($managers as $manager)
            {
                $data[]=[                
                    $manager['manager_id'], 
                    $manager['manager_salary'],
                    $manager['manager_calls'],
                    $manager['manager_bonus'],
                    $manager['manager_extra'],
                    $manager['manager_total'],                    
                    strtotime($current_month->format('Y-m-t 23:59:59')), //date last day of month
                ];
            }
            
            //save to db     
            
            if (!empty($data) && is_array($data)) {

                try{

                    $model::getDb()
                        ->createCommand()
                        ->batchInsert(
                                $model->tableName(),
                                [
                                    'manager_id',
                                    'salary',
                                    'calls',
                                    'bonus',
                                    'extra',
                                    'total',
                                    'date'
                                ],
                                $data)
                        ->execute();
                }
                catch(yii\db\Exception $e)
                {
                    throw new Exception($e->massege());
                }
            }
            
            $progress++;
            Console::updateProgress($progress, $period_count);
        }    
        
        Console::endProgress();
        
        //Console::moveCursorUp();
        $this->stdout("Apply managers payouts data.");
        $this->stdout(" [");$this->stdout("OK", Console::FG_GREEN);$this->stdout("]\n");
        
        Console::moveCursorDown();
        Console::showCursor();
    }
}