<?php

namespace app\modules\managers;

use yii\base\BootstrapInterface;

/**
 * managers module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    public $controllerNamespace = 'app\modules\managers\controllers';

    public function init()
    {
        parent::init();
        
        // custom initialization code goes here
    }

    public function bootstrap($app)
    {
        if ($app instanceof \yii\console\Application) {
        	$this->controllerNamespace = 'app\modules\managers\commands';
        }
        
        $app->getUrlManager()->addRules([
    
            'managers' => 'managers/default/index',
            'managers/payouts/<id:\d+>/<date:\d{4}-(0[1-9]{1}|1[0-2]{1})$>' => 'managers/default/history-detail',
            'managers/<a:(payouts-summary)>' => 'managers/default/<a>',            
            'managers/<a:(view|update|delete|payouts)>/<id:\d+>' => 'managers/default/<a>',    
            
        ], false);
    }
}