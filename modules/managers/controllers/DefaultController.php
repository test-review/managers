<?php

namespace app\modules\managers\controllers;

use Yii;
use app\modules\managers\models;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * DefaultController implements the CRUD actions for Managers model.
 */
class DefaultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Managers models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $searchModel = new models\ManagersSearch();
        $dataProvider = $searchModel->search(\yii::$app->request->queryParams);
        
        $status = ArrayHelper::map(models\base\ManagersStatus::find()->asArray()->all(), 'id', 'name');
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'status'=>$status,
        ]);
    }

    /**
     * Displays a single Managers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        $searchModel = new models\ManagersHistoryDetailSearch([
            'manager_id'=>$model->id,
            'date_str'=>date('Y-m')
        ]);  
        
        $dataProvider = $searchModel->search(\yii::$app->request->queryParams);                
        
        $bonus = ArrayHelper::map(models\base\ManagersBonus::find()->asArray()->all(), 'id', 'name');
        $status = ArrayHelper::map(models\base\ManagersStatus::find()->asArray()->all(), 'id', 'name');        
        
        //Overall statistics, except for the current month
        $statisticsOverall = models\base\ManagersPayouts::find()
                ->select([
                    'SUM(`calls`) `calls`',
                    'SUM(`bonus`) `bonus`',
                    'SUM(`extra`) `extra`',
                    'SUM(`total`) `total`',
                ])
                ->where(['manager_id'=>$model->id])
                ->groupBy('manager_id')
                ->one();
        
        //Current month statistics        
        $statisticsCurrent = models\base\ManagersDefaultView::find() 
                ->where(['manager_id'=>$model->id])
                ->one();
        
        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'bonus' => $bonus,
            'status' => $status,
            'so' => $statisticsOverall,
            'sc' => $statisticsCurrent
        ]);
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function actionPayouts($id)
    {
        $model = $this->findModel($id);

        $searchModel = new models\ManagersPayoutsSearch(['manager_id'=>$model->id]);  
        $dataProvider = $searchModel->search(\yii::$app->request->queryParams);                

        return $this->render('payouts', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function actionHistoryDetail($id, $date)
    {
        $model = $this->findModel($id);
        
        $searchModel = new models\ManagersHistoryDetailSearch([
            'manager_id'=>$model->id, 
            'date_str'=>$date]);
        
        $dataProvider = $searchModel->search(\yii::$app->request->queryParams);        

        $status = ArrayHelper::map(models\base\ManagersStatus::find()->asArray()->all(), 'id', 'name');          

        return $this->render('history_detail', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'status' => $status,
            'date' => $date,
        ]);
    }
    
    /**
     * 
     * @return type
     */
    public function actionPayoutsSummary()
    {
        $searchModel = new models\ManagersPayoutsSummarySearch();
        $dataProvider = $searchModel->search(\yii::$app->request->queryParams);
        
        $status = ArrayHelper::map(models\base\ManagersStatus::find()->asArray()->all(), 'id', 'name');
        
        return $this->render('payouts_summary', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'status'=>$status,
        ]);
    }

    /**
     * Updates an existing Managers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Managers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect('/'.\Yii::$app->controller->module->id);
    }

    /**
     * Finds the Managers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Managers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = models\base\Managers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
