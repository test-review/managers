<?php

use yii\db\Migration;

/**
 * Class m180604_094022_managers
 */
class m180604_094022_managers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {     
        $tableOptions = null;
        
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        else
        {
            echo $this->db->driverName.' not supported, yet.';
            return false;
        }
        
        // Managers status
        
        $this->createTable('managers_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        // Managers

        $this->createTable('managers', [
            'id' => $this->primaryKey(),
            'status_id' => $this->integer()->null(),
            'name' => $this->string()->notNull(),
            's_name' => $this->string()->null(),
            'l_name' => $this->string()->null(),
            
        ], $tableOptions);
        
        $this->createIndex(
            'manager_status_idx',
            'managers',
            'status_id'
        );
        
        $this->addForeignKey('manager_status_idx','managers', 'status_id', 'managers_status', 'id', 'CASCADE');
        
        // Managers salary
        
        $this->createTable('managers_salary', [
            'id' => $this->primaryKey(),
            'manager_id' => $this->integer()->notNull(),
            'value' => $this->decimal(10,2)->notNull(),
            'date' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'manager_salary_idx',
            'managers_salary',
            'manager_id'
        );
        
        $this->addForeignKey('manager_salary_idx','managers_salary', 'manager_id', 'managers', 'id', 'CASCADE');
        
        
        // Managers payouts
        
        $this->createTable('managers_payouts', [
            'id' => $this->primaryKey(),
            'manager_id' => $this->integer()->notNull(),
            'salary' => $this->decimal(10,2)->notNull(),
            'calls' => $this->integer()->notNull(),
            'bonus' => $this->decimal(10,2)->null(),
            'extra' => $this->decimal(10,2)->null(),
            'total' => $this->decimal(10,2)->notNull(),
            'date' => $this->integer()->notNull(),
        ], $tableOptions);
        
        $this->createIndex(
            'manager_payouts_idx',
            'managers_payouts',
            'manager_id'
        );
        
        $this->addForeignKey('manager_payouts_idx','managers_payouts', 'manager_id', 'managers', 'id', 'CASCADE');
        
        
        // Managers bonus

        $this->createTable('managers_bonus', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'value' => $this->decimal(10,2)->notNull(),
        ], $tableOptions);
        
        
        // Managers bonus category

        $this->createTable('managers_bonus_extra_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);


        //Managers bonus

        $this->createTable('managers_bonus_extra', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'value' => $this->integer()->notNull(),
            'extra' => $this->decimal(10,2)->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'manager_bonus_extra_category_idx',
            'managers_bonus_extra',
            'category_id'
        );

        $this->addForeignKey('manager_bonus_extra_category_idx','managers_bonus_extra', 'category_id', 'managers_bonus_extra_category', 'id', 'CASCADE');

        
        //Managers history

        $this->createTable('managers_history', [
            'id' => $this->primaryKey(),
            'manager_id' => $this->integer()->notNull(),
            'status_id' => $this->integer()->notNull(),
            'calls' => $this->integer()->notNull()->defaultValue(0),
            'date' => $this->integer(),
        ], $tableOptions);

        $this->createIndex(
            'manager_idx',
            'managers_history',
            'manager_id'
        );
        
        $this->addForeignKey('manager_idx','managers_history', 'manager_id', 'managers', 'id', 'CASCADE');
        
        $this->createIndex(
            'manager_history_status_idx',
            'managers_history',
            'status_id'
        );
        
        $this->addForeignKey('manager_history_status_idx','managers_history', 'status_id', 'managers_status', 'id', 'CASCADE');
        
        $this->createIndex(
            'manager_history_date_idx',
            'managers_history',
            'date'
        );
        
        $this->execute(file_get_contents(__DIR__.'/../sql/ManagersDefaultView.sql'));
        $this->execute(file_get_contents(__DIR__.'/../sql/ManagersPayoutsView.sql'));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropForeignKey('manager_status_idx', 'managers');
        $this->dropForeignKey('manager_payouts_idx', 'managers_payouts');
        $this->dropForeignKey('manager_salary_idx', 'managers_salary');
        $this->dropForeignKey('manager_idx', 'managers_history');
        $this->dropForeignKey('manager_history_status_idx', 'managers_history');
        $this->dropForeignKey('manager_bonus_extra_category_idx','managers_bonus_extra');
         
        $this->dropTable('managers');
        $this->dropTable('managers_status');
        $this->dropTable('managers_salary');
        $this->dropTable('managers_payouts');
        $this->dropTable('managers_history');
        $this->dropTable('managers_bonus');
        $this->dropTable('managers_bonus_extra');
        $this->dropTable('managers_bonus_extra_category');        
        
        $this->execute('DROP VIEW IF EXISTS managers_default_view;');
        $this->execute('DROP VIEW IF EXISTS managers_payouts_view;'); 
    }
}