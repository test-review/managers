# MANAGERS #

Test task call center managers (extended)

### The objectives of the test task ###

* Using the yii2 framework
* Database architecture development
* Staff salary statistics
* History of calls and accrued bonuses
* [Online preview](http://wc.test.twg.su:81)
*(login as admin to activate managers menu...)*

### Requirements ###

* Yii2 basic template
* MySQL v5.7 or higher

### Installation ###

* [Download repository](https://bitbucket.org/test-review/managers/downloads/)
* Unzip the files to the root of your project, module path must be **app/modules/managers**
* Add module to application configuration **app/config/web.php**
#
```javascript
		$config = [
			'bootstrap' => [
				'managers'
			],
			'modules' => [        
				'managers' => [
					'class' => 'app\modules\managers\Module',
				],
			],
			'components' => [
				...
				'urlManager' => [
					'enablePrettyUrl' => true,
					'showScriptName' => false,
					'rules' => [

					],
				],
			],
			...
		]	
```
#
* Add module to console configuration **app/config/console.php**
#
```javascript
		$config = [
			'bootstrap' => [
				'managers'
			],
			'modules' => [        
				'managers' => [
					'class' => 'app\modules\managers\Module',
				],
			],
			...
		]	
```
#
* Run console command to migrate db (only mysql support)
#
~~~~
		php yii migrate --migrationPath=@app/modules/managers/migrations
~~~~
#
* Run console command to generate data and fill database
#
~~~~
		php yii managers/db/fill-extended
~~~~
> *(By default, 50 managers are added to the database and a history is generated from January 2015)*
#
* Try to go on `http://your-project/managers`
#
#